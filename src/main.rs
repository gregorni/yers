use std::env;

fn main() {
    let passed_args = env::args().skip(1).collect::<Vec<_>>();

    loop {
        println!("{}", {
            if !passed_args.is_empty() {
                passed_args.join(" ")
            } else {
                "y".to_string()
            }
        });
    }
}
